export const REEL_WIDTH = 160;
export const SYMBOL_SIZE = 150;
export const FIRST_POSITION = 0
export const SECOND_POSITION = 150
export const THIRD_POSITION = 300
export const AMOUNT_OF_COLUMNS = 3

export  const buttonStyle = new PIXI.TextStyle({
  fontFamily: "Verdana, Geneva, sans-seri",
    fontSize: 34,
    fontStyle: "italic",
    fontWeight: "bold",
    fill: "#ffffff",
    stroke: "#4a1850",
  });

  export  const winStyle = new PIXI.TextStyle({
    fontFamil: "Impact, Charcoal, sans-seri",
      fontSize: 45,
      fontStyle: "italic",
      fontWeight: "bold",
      fill: [
        "#1de60f",
        "#cdff1a",
        "#f4e53e"
      ],

      fontStyle: "oblique",
      fontVariant: "small-caps"
    });
