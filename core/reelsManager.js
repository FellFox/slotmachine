import {
  SYMBOL_SIZE,
  REEL_WIDTH,
  AMOUNT_OF_COLUMNS
} from "../constants.js";

// Reels manager responsible for creating and updating symbols on the reels

export default class ReelsManager {
  constructor() {

    this.slotTextures = [
      PIXI.Texture.from("lemon"),
      PIXI.Texture.from("plum"),
      PIXI.Texture.from("orange"),
    ];

    this.reels = [];
    this.reelContainer = new PIXI.Container();
  }



  createReels() {
    for (let i = 0; i < AMOUNT_OF_COLUMNS; i++) {
      const rc = new PIXI.Container();
      rc.x = i * REEL_WIDTH;
      this.reelContainer.addChild(rc);
      const reel = new Reel(rc, this.slotTextures);
      reel.createSymbols();
      this.reels.push(reel);
    }
  }

  updateTextures() {
    for (let i = 0; i < this.reels.length; i++) {
      const reel = this.reels[i];
      reel.updateSymbols();
    }
  }
}

class Reel {
  constructor(container, slotTextures) {
    this.container = container;
    this.slotTextures = slotTextures;
    this.symbols = [];
    this.position = 0;
    this.previousPosition = 0;
  }

  createSymbols() {
    for (let j = 0; j < 4; j++) {
      const symbol = new PIXI.Sprite(
        this.slotTextures[Math.floor(Math.random() * this.slotTextures.length)]
      );

      symbol.y = j * SYMBOL_SIZE;
      symbol.scale.x = symbol.scale.y = Math.min(
        SYMBOL_SIZE / symbol.width,
        SYMBOL_SIZE / symbol.height
      );
      symbol.x = Math.round((SYMBOL_SIZE - symbol.width) / 2);

      this.symbols.push(symbol);
      this.container.addChild(symbol);
    }
  }

  updateSymbols() {
    this.previousPosition = this.position;
    for (let j = 0; j < this.symbols.length; j++) {
      const symbol = this.symbols[j];
      const prevy = symbol.y;
      symbol.y =
        ((this.position + j) % this.symbols.length) * SYMBOL_SIZE -
        SYMBOL_SIZE;
      if (symbol.y < 0 && prevy > SYMBOL_SIZE) {
        symbol.texture =
          this.slotTextures[Math.floor(Math.random() * this.slotTextures.length)];
        symbol.scale.x = symbol.scale.y = Math.min(
          SYMBOL_SIZE / symbol.texture.width,
          SYMBOL_SIZE / symbol.texture.height
        );
        symbol.x = Math.round((SYMBOL_SIZE - symbol.width) / 2);
      }
    }
  }
}
