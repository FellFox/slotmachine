import {
  FIRST_POSITION,
  SECOND_POSITION,
  THIRD_POSITION,
} from "../constants.js";

// Rotation function for spinning and stopping the reels

class Rotator {
  constructor(reels) {
    this.reels = reels;
  }

  async spin() {
    const promises = [];
    for (let i = 0; i < this.reels.length; i++) {
      const r = this.reels[i];
      const extra = Math.floor(Math.random() * 3);
      const target = r.position + 10 + i * 5 + extra;
      promises.push(
        gsap.to(r, {
          position: target,
          duration: 2 + i / 2,
        })
      );
    }

    await Promise.all(promises);
  }

  async stop() {
    const promises = [];
    for (let i = 0; i < this.reels.length; i++) {
      const r = this.reels[i];
      promises.push(gsap.killTweensOf(r));
      const targetPositions = [FIRST_POSITION, SECOND_POSITION, THIRD_POSITION];
      const currentPosition = r.position % 360;
      const distances = targetPositions.map((pos) =>
        Math.abs(pos - currentPosition)
      );
      const closestIndex = distances.indexOf(Math.min(...distances));
      const closestPosition = targetPositions[closestIndex];
      r.position = closestPosition;
    }

    await Promise.all(promises);
  }
}

export default Rotator;
