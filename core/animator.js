import { SYMBOL_SIZE } from "../constants.js";

// Animation class responsible for creating, adding and removing animations from the stage

class Animation {
  constructor(textures, frameDuration) {
    this.sprite = new PIXI.AnimatedSprite(textures);
    this.sprite.animationSpeed = 1 / frameDuration;
    this.sprite.play();
  }

  get animation() {
    return this.sprite;
  }

}

export default class AnimationsBuilder {
  constructor(rows, activeAnimations, atlas, winningLines) {
    this.rows = rows;
    this.activeAnimations = activeAnimations;
    this.atlas = atlas;
    this.winningLines = winningLines;
    this.animationTypes = {
      lemon: this.createTextures("sym_lemon"),
      orange: this.createTextures("sym_orange"),
      plum: this.createTextures("sym_plum"),
    };
  }

  createTextures(name) {
    return this.atlas[name].map((filename) =>
      PIXI.Texture.from(filename.textureCacheIds[0])
    );
  }

  clearAnimations() {
    this.activeAnimations.forEach((activeAnimation) => {
      const parent = activeAnimation.sprite.parent;
      parent.removeChild(activeAnimation.animation);
      parent.addChild(activeAnimation.symbol);
    });
    this.activeAnimations.length = 0;
  }

  createAnimations() {
    this.winningLines.forEach((line) => {
      const winningLine = this.rows[line];
      winningLine.map((symbol) => {
        const animation = new Animation(
          this.animationTypes[symbol.texture.textureCacheIds[0]],
          7
        );
        animation.animation.scale.x = animation.animation.scale.y = Math.min(
          SYMBOL_SIZE / animation.animation.width,
          SYMBOL_SIZE / animation.animation.height
        );
        animation.animation.y = symbol.y;
        animation.animation.x = symbol.x;

        animation.symbol = symbol;

        animation.symbol.parent.addChild(animation.animation);
        animation.symbol.parent.removeChild(animation.symbol);
        this.activeAnimations.push(animation);
      });
    });
    this.winningLines.shift();
  }
}
