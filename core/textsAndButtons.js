import { SYMBOL_SIZE, buttonStyle, winStyle } from "../constants.js";

// Class for creating, adding and updating the Texts and the Playbutton of the game.

export default class TextsAndButtons {
  constructor(screen) {
    this.margin = (screen.height - SYMBOL_SIZE * 3) / 2;

    this.topContainer = new PIXI.Graphics();
    this.topContainer.beginFill(0, 1);
    this.topContainer.drawRect(0, 0, screen.width, this.margin);

    this.bottomContainer = new PIXI.Graphics();
    this.bottomContainer.beginFill(0, 1);
    this.bottomContainer.drawRect(
      0,
      SYMBOL_SIZE * 3 + this.margin,
      screen.width,
      this.margin
    );

    this.playButton = new PIXI.Graphics();
    this.playButton.lineStyle(2, 0xffffff);
    this.playButton.beginFill(0x00ff00);
    this.playButton.drawRoundedRect(
      -100,
      235,
      200,
      this.bottomContainer.height - 20,
      20
    );
    this.playButton.x = screen.width / 2;
    this.playButton.y = screen.height / 2;
    this.playButton.interactive = true;
    this.playButton.buttonMode = true;

    this.goText = new PIXI.Text("GO", buttonStyle);
    this.goText.position.set(
      this.bottomContainer.width / 2 - this.goText.width / 2,
      screen.height - this.goText.height * 1.4
    );

    this.stopText = new PIXI.Text("STOP", buttonStyle);
    this.stopText.position.set(
      this.bottomContainer.width / 2 - this.stopText.width / 2,
      screen.height - this.stopText.height * 1.4
    );

    this.winText = new PIXI.Text("You Win !", winStyle);
    this.winText.x = Math.round((this.topContainer.width - this.winText.width) / 2);
    this.winText.y = Math.round((this.margin - this.winText.height) / 2);

    this.bottomContainer.addChild(this.playButton);
    this.bottomContainer.addChild(this.goText);
  }

  
  updateTextsAndButton(running) {
    this.bottomContainer.removeChild(running ? this.goText : this.stopText);
    this.bottomContainer.addChild(running ? this.stopText : this.goText);

    const buttonColor = running ? 0xff0000 : 0x00ff00;
  
    this.playButton.clear();
    this.playButton.beginFill(buttonColor);
    this.playButton.drawRoundedRect(
      -100,
      235,
      200,
      this.bottomContainer.height - 20,
      20
    );

    this.playButton.endFill();

    if (running) {
      this.topContainer.removeChild(this.winText);
    }
  }
  }

//   updateTopText(running) {
// }