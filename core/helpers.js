export function allElementsEqual(arr) {
  return arr.every((el) => el === arr[0]);
}
