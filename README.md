## Slot Game
This is a simple slot game built using HTML, JavaScript, PIXI.JS and GSAP.

### Getting Started
Clone the repository to your local machine:

git clone https://gitlab.com/FellFox/slotmachine.git

Open game on any live server to play.

### How to Play
 * Click the "Spin" button to start the game.
 Watch the reels spin and see if you get a winning combination.
 * Click the "Stop" button to stop the reels from spinning at any time.


* If you get a winning combination, you will see the "You Win!" message and the winning animations.

### Customization
You can customize the game by:
 *  changing the images used for symbols in the reels
 *  changing the amount of reels (up to 5)

