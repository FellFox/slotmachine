import Animator from "./core/animator.js";
import Rotator from "./core/rotator.js";
import TextsAndButtons from "./core/textsAndButtons.js";
import ReelsManager from "./core/reelsManager.js";
import { allElementsEqual } from "./core/helpers.js";
import {
  REEL_WIDTH,
  AMOUNT_OF_COLUMNS,
  FIRST_POSITION,
  SECOND_POSITION,
  THIRD_POSITION,
} from "./constants.js";

class SlotMachine {
  constructor() {
    const app = new PIXI.Application();
    document.body.appendChild(app.view);

    this.rows = [];
    this.activeAnimations = [];
    this.winningLines = [];
    this.running = false;

    const loader = new PIXI.Loader();

    loader
      .add("lemon", "assets/sym_lemon.png")
      .add("plum", "assets/sym_plum.png")
      .add("orange", "assets/sym_orange.png")
      .add("spritesheet", "assets/sym_anim.json")
      .add("image", "assets/sym_anim.png")
      .add("background", "assets/bg.jpg");

    loader.load(this.onAssetsLoaded.bind(this));

    this.app = app;
    this.loader = loader;
  }

  onAssetsLoaded() {
    this.createBackground();
    this.createTextsAndButtons();
    this.createReels();
    this.createAnimator();
    this.createListeners();
    this.resizeApp();
  }

  createBackground() {
    const backgroundTexture = PIXI.Texture.from("background");
    const background = new PIXI.Sprite(backgroundTexture);
    this.app.stage.addChild(background);
  }

  createAnimator() {
    const atlas = this.loader.resources.spritesheet.spritesheet.animations;
    this.animator = new Animator(
      this.rows,
      this.activeAnimations,
      atlas,
      this.winningLines
    );
  }
  createTextsAndButtons() {
    const textsAndButtons = new TextsAndButtons(this.app.screen);
    const top = textsAndButtons.topContainer;
    const bottom = textsAndButtons.bottomContainer;
    const playButton = textsAndButtons.playButton;

    this.top = top;
    this.bottom = bottom;
    this.playButton = playButton;
    this.textsAndButtons = textsAndButtons;
  }

  createReels() {
    const reelsManager = new ReelsManager();
    reelsManager.createReels();

    const reels = reelsManager.reels;
    const reelsContainer = reelsManager.reelContainer;
    const rotator = new Rotator(reels);

    reelsContainer.y = this.top.height;
    reelsContainer.x = Math.round(
      (this.app.screen.width - REEL_WIDTH * AMOUNT_OF_COLUMNS) / 2
    );

    this.app.stage.addChild(reelsContainer);
    this.app.stage.addChild(this.top);
    this.app.stage.addChild(this.bottom);

    this.reels = reels;
    this.reelsManager = reelsManager;
    this.rotator = rotator;
  }

  createListeners() {
    this.playButton.addListener("pointerdown", () =>
      this.running == false ? this.startPlay() : this.stopPlay()
    );

    this.playButton.addListener("pointerover", () => {
      this.playButton.alpha = 0.7;
    });

    this.playButton.addListener("pointerout", () => {
      this.playButton.alpha = 1;
    });

    this.app.ticker.add(() => {
      this.reelsManager.updateTextures();
    });
    
    this.resizeApp = this.resizeApp.bind(this);
    window.addEventListener("resize", this.resizeApp);
  }

  async startPlay() {
    this.running = true;
    this.textsAndButtons.updateTextsAndButton(this.running);
    this.animator.clearAnimations();
    this.winningLines.length = 0;
    this.rows.forEach((el) => (el.length = 0));
    this.rows.length = 0;

    await this.rotator.spin();
    this.running = false;
    this.textsAndButtons.updateTextsAndButton(this.running);
    setTimeout(() => {
      this.checkForWin(this.rotator.reels, this.rows);
    }, 50);
  }

  async stopPlay() {
    this.running = false;
    this.textsAndButtons.updateTextsAndButton(this.running);
    
    await this.rotator.stop();
    setTimeout(() => {
      this.checkForWin(this.rotator.reels, this.rows);
    }, 50);
  }

  checkForWin(reels, rows) {

    const firstRow = [];
    const secondRow = [];
    const thirdRow = [];

    reels.map((reel) => {
      reel.symbols.map((symbol) => {
        if (symbol.transform.position.y == FIRST_POSITION) {
          firstRow.push(symbol);
        } else if (symbol.transform.position.y == SECOND_POSITION) {
          secondRow.push(symbol);
        } else if (symbol.transform.position.y == THIRD_POSITION) {
          thirdRow.push(symbol);
        }
      });
    });
    
    rows.push(firstRow, secondRow, thirdRow);

    rows.forEach((row, index) => {
      if (
        allElementsEqual(row.map((symbol) => symbol.texture.textureCacheIds[0]))
      ) {
        this.winningLines.push(index);
        const winText = this.textsAndButtons.winText;
        this.top.addChild(winText);
        this.animator.createAnimations();
      }
    });
  }

  resizeApp() {
    const xOffset = (window.innerWidth - this.app.stage.width) / 2;
    const yOffset = (window.innerHeight - this.app.stage.height) / 2;

    this.app.view.style.position = "absolute";
    this.app.view.style.left = xOffset + "px";
    this.app.view.style.top = yOffset + "px";
  }
}

new SlotMachine();
